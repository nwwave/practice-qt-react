#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtGui>
#include <QAxObject>

QString file("G:\\Test.xlsx"); //Наш файл

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->pushButton, &QPushButton::clicked, this, &MainWindow::readExcel);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::readExcel()
{
    // с помощью QAxObject получаем указатель на Excel (COM-объект)
    QAxObject* excel = new QAxObject("Excel.Application",this);
    QAxObject* workbooks = excel->querySubObject("Workbooks"); // рабочая книга
    QAxObject* workbook = workbooks->querySubObject("Open(const QString&)", file);
    excel->dynamicCall("SetVisible(bool)",false); // видимость документа

    QAxObject *worksheet = workbook->querySubObject("WorkSheets(int)",1);

    //получаем кол-во используемых строк и столбцов
    QAxObject * usedrange = worksheet->querySubObject("UsedRange");
    QAxObject *rows = usedrange->querySubObject("Rows");
    QAxObject *columns = usedrange->querySubObject("Columns");

    int intRowStart = usedrange->property("Row").toInt();
    int intColStart = usedrange->property("Column").toInt();
    int intCols = columns->property("Count").toInt();
    int intRows = rows->property("Count").toInt();

    qDebug()<<intRows; // кол-во строк (с данными)
    qDebug()<<intCols; // кол-во столбцов

    ui->tableWidget->setColumnCount(intColStart + intCols);
    ui->tableWidget->setRowCount(intRowStart + intRows);

    //зная кол-во строк и столбцов заполняем таблицу
    for (int row=0; row<intRows; row++)
    {
        for(int col=0; col<intCols; col++)
        {
            QAxObject* cell = worksheet->querySubObject("Cells(int,int)", row+1, col+1);
            QVariant value = cell->dynamicCall("Value()");
            QTableWidgetItem *item = new QTableWidgetItem(value.toString());
            ui->tableWidget->setItem(row,col,item);
        }
    }
    //закрываем документ и выходим
    workbook->dynamicCall("Close");
    excel->dynamicCall("Quit()");
}

