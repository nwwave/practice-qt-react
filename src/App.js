import React, { useState, useRef, useEffect, useCallback } from "react";

const AppWs = () => {
  const [isPaused, setIsPaused] = useState(false);
  const [data, setData] = useState(null);
  const [status, setStatus] = useState("");
  const ws = useRef(null);

  useEffect(() => {
    if (!isPaused) {
      ws.current = new WebSocket("ws://localhost:3000/ws"); // создаем ws соединение
      ws.current.onopen = () => setStatus("Соединение открыто");  // callback на ивент открытия соединения
      ws.current.onclose = () => setStatus("Соединение закрыто"); // callback на ивент закрытия соединения

      gettingData();
    }

    return () => ws.current.close(); // кода меняется isPaused - соединение закрывается
  }, [ws, isPaused]);

  const gettingData = useCallback(() => {
    if (!ws.current) return;

    ws.current.onmessage = e => {                //подписка на получение данных по вебсокету
      if (isPaused) return;
      const message = JSON.parse(e.data);
      setData(message);
    };
  }, [isPaused]);

    var debugTextArea = document.getElementById("debugTextArea");
    function debug(message) {
        debugTextArea.value += message + "\n";
        debugTextArea.scrollTop = debugTextArea.scrollHeight;
    }

    function sendMessage() {
        var msg = document.getElementById("inputText").value;
        if ( WebSocket != null )
        {
            document.getElementById("inputText").value = "";
            WebSocket.send( msg );
            console.log( "string sent :", '"'+msg+'"' );
        }
    }

    var debugTextArea = document.getElementById("debugTextArea");
    function debug(message) {
        debugTextArea.value += message + "\n";
        debugTextArea.scrollTop = debugTextArea.scrollHeight;
    }

  return (
      <>
        {!!data &&
        <div>
          <div>
            <h2>{status}</h2>
            <p>{`connection ID: ${data?.connectionID}`}</p>
            <p>{`event: ${data?.event}`}</p>
            <p>{`status: ${data?.status}`}</p>
            <p>{`version: ${data?.version}`}</p>

          </div>
          <button onClick={() => {
            ws.current.close();
            setIsPaused(!isPaused)
          }}>{!isPaused ? 'Остановить соединение' : 'Открыть соединение' }</button>
            <p>
                <textarea id="debugTextArea"></textarea>
            </p>
            <p>
                <input type="text" id="inputText" onKeyDown={sendMessage}/>
                <button onClick="sendMessage();">Send</button>
            </p>
        </div>
        }
      </>
  )
}

export default AppWs;
